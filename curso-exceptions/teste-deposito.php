<?php

require_once 'autoload.php';

use Alura\Banco\Modelo\Conta\{
    ContaCorrente,
    Titular};
use Alura\Banco\Modelo\{CPF, Endereco};

$contaCorrente = new ContaCorrente(
    new Titular(
        new CPF('123.456.789-10'),
        'Vinicius Dias',
        new Endereco('Petrópolis', 'bairro Teste', 'Rua lá', '37')
    )
);

try {
    $contaCorrente->deposita(-100);
} catch (InvalidArgumentException $exception) {
    echo "O valor precisa ser positivo.";
}