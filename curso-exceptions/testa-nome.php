<?php

require_once 'autoload.php';

use Alura\Banco\Modelo\Conta\Titular;
use Alura\Banco\Modelo\Endereco;
use Alura\Banco\Modelo\CPF;
use Alura\Banco\Modelo\Funcionario\ValidaNomeException;

$endereco = new Endereco('Petrópolis', 'um bairro', 'minha rua', '71B');

try {
    $vinicius = new Titular(new CPF('123.456.789-10'), 'Dias', $endereco);
} catch (ValidaNomeException $exception) {
    echo $exception->getMessage();
}

